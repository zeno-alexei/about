function zero_pad(value, length) {
    return value.toString().padStart(length, '0');
}

function calculate_age(date, birthdate) {
    const TOTAL_CENTISECONDS = Math.floor((date - birthdate) / 10);
    const TOTAL_SECONDS = Math.floor(TOTAL_CENTISECONDS / 100);
    const TOTAL_MINUTES = Math.floor(TOTAL_SECONDS / 60);
    const TOTAL_HOURS = Math.floor(TOTAL_MINUTES / 60);
    const TOTAL_DAYS = Math.floor(TOTAL_HOURS / 24);

    const CENTISECONDS = TOTAL_CENTISECONDS % 100;
    const SECONDS = TOTAL_SECONDS % 60;
    const MINUTES = TOTAL_MINUTES % 60;
    const HOURS = TOTAL_HOURS % 24;
    const DAYS = TOTAL_DAYS % 365;
    const YEARS = Math.floor(TOTAL_DAYS / 365);

    return {
        YEARS,
        DAYS,
        HOURS,
        MINUTES,
        SECONDS,
        CENTISECONDS
    };
}

function display_age(time) {
    const FORMATTED_AGE = `${zero_pad(time.YEARS, 2)}:${zero_pad(time.DAYS, 3)}:${zero_pad(time.HOURS, 2)}:${zero_pad(time.MINUTES, 2)}:${zero_pad(time.SECONDS, 2)}:${zero_pad(time.CENTISECONDS, 2)}`;
    document.getElementById('age').textContent = FORMATTED_AGE;
}

document.addEventListener('DOMContentLoaded', () => {
    const BIRTHDATE = new Date(2006, 8, 3).getTime();  // 2006 09 03 (month is indexed 0-11)
    let AGE = calculate_age(new Date().getTime(), BIRTHDATE);

    display_age(AGE);

    // recalculate every second
    setInterval(() => {
        AGE = calculate_age(new Date().getTime(), BIRTHDATE);
    }, 1000);

    // increment centiseconds accurately
    setInterval(() => {
        AGE.CENTISECONDS += 1;
        if (AGE.CENTISECONDS >= 100) {
            AGE.CENTISECONDS = 0;
            AGE.SECONDS += 1;
        }
        if (AGE.SECONDS >= 60) {
            AGE.SECONDS = 0;
            AGE.MINUTES += 1;
        }
        if (AGE.MINUTES >= 60) {
            AGE.MINUTES = 0;
            AGE.HOURS += 1;
        }
        if (AGE.HOURS >= 24) {
            AGE.HOURS = 0;
            AGE.DAYS += 1;
        }
        if (AGE.DAYS >= 365) {
            AGE.DAYS = 0;
            AGE.YEARS += 1;
        }
        display_age(AGE);
    }, 10);
});
